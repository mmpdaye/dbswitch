// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.gitee.dbswitch.product.mariadb;

import com.gitee.dbswitch.features.ProductFeatures;

public class MariadbFeatures implements ProductFeatures {

  public int convertFetchSize(int fetchSize) {
    return Integer.MIN_VALUE;
  }

}
